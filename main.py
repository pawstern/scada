# -*- coding: utf-8 -*-
import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from modbus import Modbus
from math import ceil
import threading

class Main(QWidget):

    timeout = 1

    spnwnState = False
    spnwwState = False
    zpnwState = False
    zwnState = False
    zwwState = False
    
    def __init__(self):
        super().__init__()
        
        self.initUI()

    def initUI(self):

        self.error_dialog = QMessageBox(self)
        self.error_dialog.setText("CRC error")

        portLabel = QLabel("Port", self)
        portLabel.move(90,20)
        self.portTextBox = QLineEdit(self)
        self.portTextBox.move(20,20)
        self.portTextBox.resize(60,15)
        self.portTextBox.setText('COM5')

        baudRateLabel = QLabel("BaudRate", self)
        baudRateLabel.move(90,40)
        self.baudRateTextBox = QLineEdit(self)
        self.baudRateTextBox.move(20,40)
        self.baudRateTextBox.resize(60,15)
        self.baudRateTextBox.setText('9600')

        dataBitsLabel = QLabel("DataBits", self)
        dataBitsLabel.move(90,60)
        self.dataBitsTextBox = QLineEdit(self)
        self.dataBitsTextBox.move(20,60)
        self.dataBitsTextBox.resize(60,15)
        self.dataBitsTextBox.setText('8')

        stopBitsLabel = QLabel("StopBits", self)
        stopBitsLabel.move(90,80)
        self.stopBitsTextBox = QLineEdit(self)
        self.stopBitsTextBox.move(20,80)
        self.stopBitsTextBox.resize(60,15)
        self.stopBitsTextBox.setText('1')
        

        modbusAddressLabel = QLabel("ModbusAddress", self)
        modbusAddressLabel.move(90,100)
        self.modbusAddressTextBox = QLineEdit(self)
        self.modbusAddressTextBox.move(20,100)
        self.modbusAddressTextBox.resize(60,15)
        self.modbusAddressTextBox.setText('7')


        
        self.openPortBtn = QPushButton('Open Port', self)
        self.openPortBtn.clicked.connect(lambda: self.openConnection(
                                        self.portTextBox.text(),
                                        int(self.baudRateTextBox.text()),
                                        int(self.dataBitsTextBox.text()),
                                        int(self.stopBitsTextBox.text()),
                                        self.timeout,
                                        int(self.modbusAddressTextBox.text())))
        self.openPortBtn.move(180, 50)

        self.changeConnectionBtn = QPushButton('Change parameters', self)
        self.changeConnectionBtn.clicked.connect(lambda: self.changeConnectionParameters(
                                        self.portTextBox.text(),
                                        int(self.baudRateTextBox.text()),
                                        int(self.dataBitsTextBox.text()),
                                        int(self.stopBitsTextBox.text()),
                                        self.timeout,
                                        int(self.modbusAddressTextBox.text())))
        self.changeConnectionBtn.move(180, 80)
        self.changeConnectionBtn.setEnabled(False)

        readDataBtn = QPushButton('Read', self)
        readDataBtn.clicked.connect(lambda: threading.Thread(target=self.readData).start())
        readDataBtn.move(240, 530)

        self.sznwTextBox = QLineEdit(self)
        self.sznwTextBox.move(250,305)
        self.sznwTextBox.resize(40,20)
        self.sznwTextBox.setText('0')

        self.pownTextBox = QLineEdit(self)
        self.pownTextBox.move(250,325)
        self.pownTextBox.resize(40,20)
        self.pownTextBox.setText('0')

        self.powwTextBox = QLineEdit(self)
        self.powwTextBox.move(250,345)
        self.powwTextBox.resize(40,20)
        self.powwTextBox.setText('0')

        self.spowkTextBox = QLineEdit(self)
        self.spowkTextBox.move(250,365)
        self.spowkTextBox.resize(40,20)
        self.spowkTextBox.setText('0')

        sznwBtn = QPushButton('Ustaw', self)
        sznwBtn.clicked.connect(self.sznwSet)
        sznwBtn.move(330, 305)

        pownBtn = QPushButton('Ustaw', self)
        pownBtn.clicked.connect(self.pownSet)
        pownBtn.move(330, 325)

        powwBtn = QPushButton('Ustaw', self)
        powwBtn.clicked.connect(self.powwSet)
        powwBtn.move(330, 345)

        spowkBtn = QPushButton('Ustaw', self)
        spowkBtn.clicked.connect(self.spowkSet)
        spowkBtn.move(330, 365)

        ctzLabel = QLabel("Czujnik temp. zewnetrznej", self)
        ctzLabel.move(20, 150)

        self.ctzValueLabel = QLabel("                          ", self)
        self.ctzValueLabel.move(300, 150)

        ctpnLabel = QLabel("Czujnik temp. powietrza nawiewanego", self)
        ctpnLabel.move(20, 170)

        self.ctpnValueLabel = QLabel("                        ", self)
        self.ctpnValueLabel.move(300, 170)

        ctpzonwLabel = QLabel("Czujnik temp. powietrza za odzyskiem na wywiewie", self)
        ctpzonwLabel.move(20, 190)

        self.ctpzonwValueLabel = QLabel("                    ", self)
        self.ctpzonwValueLabel.move(300, 190)

        tpznLabel = QLabel("Termostat p-zamr. nagrzewnicy", self)
        tpznLabel.move(20, 230)

        self.tpznValueLabel = QLabel("                      ", self)
        self.tpznValueLabel.move(300, 230)

        pfpnLabel = QLabel("Presostat filtra podstawowego nawiewu", self)
        pfpnLabel.move(20, 250)

        self.pfpnValueLabel = QLabel("                     ", self)
        self.pfpnValueLabel.move(300, 250)

        pfpwLabel = QLabel("Presostat filtra podstawowego wywiewu", self)
        pfpwLabel.move(20, 270)

        self.pfpwValueLabel = QLabel("                    ", self)
        self.pfpwValueLabel.move(300, 270)




        sznwLabel = QLabel("Siłownik zaworu nagrzewnicy wstępnej", self)
        sznwLabel.move(20, 310)

        self.sznwValueLabel = QLabel("%", self)
        self.sznwValueLabel.move(300, 310)

        pownLabel = QLabel("Prędkość obrotowa wentylatora nawiewu", self)
        pownLabel.move(20, 330)

        self.pownValueLabel = QLabel("%", self)
        self.pownValueLabel.move(300, 330)

        powwLabel = QLabel("Prędkość obrotowa wentylatora wywiewu", self)
        powwLabel.move(20, 350)

        self.powwValueLabel = QLabel("%", self)
        self.powwValueLabel.move(300, 350)

        spowkLabel = QLabel("Siłownik przep. obej. wymiennika krzyż.", self)
        spowkLabel.move(20, 370)

        self.spowkValueLabel = QLabel("%", self)
        self.spowkValueLabel.move(300, 370)





        spnwnLabel = QLabel("Siłownik przepustnicy na wlocie nawiewu", self)
        spnwnLabel.move(20, 410)

        spnwwLabel = QLabel("Siłownik przepustnicy na wylocie wywiewu", self)
        spnwwLabel.move(20, 430)

        zpnwLabel = QLabel("Załączenie pompy nagrzewnicy wstępnej", self)
        zpnwLabel.move(20, 450)

        zwnLabel = QLabel("Załączenie wentylatora nawiewu", self)
        zwnLabel.move(20, 470)

        zwwLabel = QLabel("Załączenie wentylatora wywiewu", self)
        zwwLabel.move(20, 490)



        self.spnwnBtn = QPushButton('          ', self)
        self.spnwnBtn.clicked.connect(self.spnwnSet)
        self.spnwnBtn.move(270, 405)

        self.spnwwBtn = QPushButton('          ', self)
        self.spnwwBtn.clicked.connect(self.spnwwSet)
        self.spnwwBtn.move(270, 425)

        self.zpnwBtn = QPushButton('          ', self)
        self.zpnwBtn.clicked.connect(self.zpnwSet)
        self.zpnwBtn.move(270, 445)

        self.zwnBtn = QPushButton('          ', self)
        self.zwnBtn.clicked.connect(self.zwnSet)
        self.zwnBtn.move(270, 465)

        self.zwwBtn = QPushButton('          ', self)
        self.zwwBtn.clicked.connect(self.zwwSet)
        self.zwwBtn.move(270, 485)

        self.setGeometry(0, 0, 500, 500)
        self.setWindowTitle('SCADA')    
        self.show()

    def openConnection(self, port, baudrate, bytesize, stopbits, timeout, address):
        try:
            self.modbus = Modbus(port, baudrate, bytesize, stopbits, timeout, address)
            self.readData()
            self.openPortBtn.setEnabled(False)
            self.changeConnectionBtn.setEnabled(True)
            self.mainTimer = QTimer()
            self.mainTimer.setInterval(60000)
            self.mainTimer.timeout.connect(lambda: threading.Thread(target=self.readData).start())
            self.mainTimer.start()
        except:
            self.error_dialog.open()


    def spnwnSet(self):

        try:
            self.modbus.switch_binary_register(300,0)
            if self.spnwnState:
                self.spnwnBtn.setText("off")
                self.spnwnBtn.setStyleSheet('color: red')
            
            else:
                self.spnwnBtn.setText("on")
                self.spnwnBtn.setStyleSheet('color: green')

            self.spnwnState = not self.spnwnState

        except:
            self.error_dialog.open()
            

    def spnwwSet(self):
        try:
            self.modbus.switch_binary_register(300,5)
            if self.spnwwState:
                self.spnwwBtn.setText("off")
                self.spnwwBtn.setStyleSheet('color: red')
                
            else:
                self.spnwwBtn.setText("on")
                self.spnwwBtn.setStyleSheet('color: green')

            self.spnwwState = not self.spnwwState
        except:
            self.error_dialog.open()

    def zpnwSet(self):
        try:
            self.modbus.switch_binary_register(301,0)
            if self.zpnwState:
                self.zpnwBtn.setText("off")
                self.zpnwBtn.setStyleSheet('color: red')
                
            else:
                self.zpnwBtn.setText("on")
                self.zpnwBtn.setStyleSheet('color: green')

            self.zpnwState = not self.zpnwState
        except:
            self.error_dialog.open()

    def zwnSet(self):
        try:
            self.modbus.switch_binary_register(302,0)
            if self.zwnState:
                self.zwnBtn.setText("off")
                self.zwnBtn.setStyleSheet('color: red')
                
            else:
                self.zwnBtn.setText("on")
                self.zwnBtn.setStyleSheet('color: green')

            self.zwnState = not self.zwnState
        except:
            self.error_dialog.open()

    def zwwSet(self):
        try:
            self.modbus.switch_binary_register(302,1)
            if self.zwwState:
                self.zwwBtn.setText("off")
                self.zwwBtn.setStyleSheet('color: red')
                
            else:
                self.zwwBtn.setText("on")
                self.zwwBtn.setStyleSheet('color: green')

            self.zwwState = not self.zwwState
        except:
            self.error_dialog.open()



        

    def changeConnectionParameters(self, port, baudrate, bytesize, stopbits, timeout, address):
        try:
            self.modbus.changeConnectionParameters(port, baudrate, bytesize, stopbits, timeout, address)
        except:
            self.error_dialog.open()
            
    def sznwSet(self):
        try:
            value = int(self.sznwTextBox.text())
            value = ceil(value*255/100)
            self.modbus.write_analog_register(200, value)
        except:
            self.error_dialog.open()

    def pownSet(self):
        try:
            value = int(self.pownTextBox.text())
            value = ceil(value*255/100)
            self.modbus.write_analog_register(210, value)
        except:
            self.error_dialog.open()

    def powwSet(self):
        try:
            value = int(self.powwTextBox.text())
            value = ceil(value*255/100)
            self.modbus.write_analog_register(211, value)
        except:
            self.error_dialog.open()

    def spowkSet(self):
        try:
            value = int(self.spowkTextBox.text())
            value = ceil(value*255/100)
            self.modbus.write_analog_register(223, value)
        except:
            self.error_dialog.open()

    def readData(self):
        try:
            print("Start reading")
            
            ctzValue = self.modbus.read_analog_input(10)
            self.ctzValueLabel.setText(str(ctzValue))

            ctpnValue = self.modbus.read_analog_input(12)
            self.ctpnValueLabel.setText(str(ctpnValue))

            ctpzonwValue = self.modbus.read_analog_input(18)
            self.ctpzonwValueLabel.setText(str(ctpzonwValue))

            tpznValue = self.modbus.read_binary_register(100, 0)

            if tpznValue == 0:
                self.tpznValueLabel.setText("off")
                self.tpznValueLabel.setStyleSheet('color: red')
            else:
                self.tpznValueLabel.setText("on")
                self.tpznValueLabel.setStyleSheet('color: green')

            pfpnValue = self.modbus.read_binary_register(101, 0)

            if pfpnValue == 0:
                self.pfpnValueLabel.setText("off")
                self.pfpnValueLabel.setStyleSheet('color: red')
            else:
                self.pfpnValueLabel.setText("on")
                self.pfpnValueLabel.setStyleSheet('color: green')

            pfpwValue = self.modbus.read_binary_register(101, 4)

            if pfpwValue == 0:
                self.pfpwValueLabel.setText("off")
                self.pfpwValueLabel.setStyleSheet('color: red')
            else:
                self.pfpwValueLabel.setText("on")
                self.pfpwValueLabel.setStyleSheet('color: green')


            sznwValue = self.modbus.read_analog_register(200)
            self.sznwTextBox.setText(str(round(sznwValue*100/255)))

            pownValue = self.modbus.read_analog_register(210)
            self.pownTextBox.setText(str(round(pownValue*100/255)))

            powwValue = self.modbus.read_analog_register(211)
            self.powwTextBox.setText(str(round(powwValue*100/255)))

            spowkValue = self.modbus.read_analog_register(223)
            self.spowkTextBox.setText(str(round(spowkValue*100/255)))



            spnwnValue = self.modbus.read_binary_register(300, 0)

            if spnwnValue == 0:
                self.spnwnBtn.setText("off")
                self.spnwnBtn.setStyleSheet('color: red')
                self.spnwnState = False
                
            else:
                self.spnwnBtn.setText("on")
                self.spnwnBtn.setStyleSheet('color: green')
                self.spnwnState = True

            spnwwValue = self.modbus.read_binary_register(300, 5)

            if spnwwValue == 0:
                self.spnwwBtn.setText("off")
                self.spnwwBtn.setStyleSheet('color: red')
                self.spnwwState = False
            else:
                self.spnwwBtn.setText("on")
                self.spnwwBtn.setStyleSheet('color: green')
                self.spnwwState = True

            zpnwValue = self.modbus.read_binary_register(301, 0)

            if zpnwValue == 0:
                self.zpnwBtn.setText("off")
                self.zpnwBtn.setStyleSheet('color: red')
                self.zpnwState = False
            else:
                self.zpnwBtn.setText("on")
                self.zpnwBtn.setStyleSheet('color: green')
                self.zpnwState = True

            zwnValue = self.modbus.read_binary_register(302, 0)

            if zwnValue == 0:
                self.zwnBtn.setText("off")
                self.zwnBtn.setStyleSheet('color: red')
                self.zwnState = False
            else:
                self.zwnBtn.setText("on")
                self.zwnBtn.setStyleSheet('color: green')
                self.zwnState = True

            zwwValue = self.modbus.read_binary_register(302, 1)

            if zwwValue == 0:
                self.zwwBtn.setText("off")
                self.zwwBtn.setStyleSheet('color: red')
                self.zwwState = False
            else:
                self.zwwBtn.setText("on")
                self.zwwBtn.setStyleSheet('color: green')
                self.zwwState = True

            print("End reading")
        except:
            self.error_dialog.open()
        
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    main = Main()
    sys.exit(app.exec_())

