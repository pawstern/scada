import minimalmodbus
import serial

class Modbus():
    

    def __init__(self, port, baudrate, bytesize, stopbits, timeout, address):
        self.instrument = minimalmodbus.Instrument(port, address)
        self.instrument.serial.baudrate = baudrate
        self.instrument.serial.bytesize = bytesize
        self.instrument.serial.parity   = serial.PARITY_NONE
        self.instrument.serial.stopbits = stopbits
        self.instrument.mode = minimalmodbus.MODE_RTU
        self.instrument.serial.timeout  = timeout

    def read_analog_input(self, address):
        value = self.instrument.read_register(address,0)
        lowValue = value & 0x0001F
        highValue = value & 0x0FFE0
        lowValue = round(lowValue/32,2)
        highValue = highValue/32
        return highValue + lowValue

    def read_analog_register(self, address):
        return self.instrument.read_register(address,0)


    def write_analog_register(self, address, value):
        self.instrument.write_register(address, value, 0, functioncode=6)
        return self.read_analog_register(address)

    def read_binary_register(self, address, bit):
        value = self.instrument.read_register(address,0)
        if(value&(2**bit)==0):
            return 0
        else:
            return 1

    def switch_binary_register(self, address, bit):
        value = self.instrument.read_register(address,0)
        value ^= (2**bit)
        self.instrument.write_register(address, value, 0, functioncode=6)

    def changeConnectionParameters(self, port, baudrate, bytesize, stopbits, timeout, address):
        self.instrument.port = port
        self.instrument.slaveaddress = address
        self.instrument.serial.baudrate = baudrate
        self.instrument.serial.bytesize = bytesize
        self.instrument.serial.parity   = serial.PARITY_NONE
        self.instrument.serial.stopbits = stopbits
        self.instrument.serial.timeout  = timeout





